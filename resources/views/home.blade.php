@include('header')
<div class="container">
     <div class="row">
          <div class="col-md-12">
               <div class="d-flex justify-content-center mt-5 row">

                    <div class="col-md-12 mb-5">

                         <div class="card">
                              <h3 class="card-header light-blue lighten-1 white-text text-uppercase font-weight-bold text-center py-3">
                                   TIC TAC TOE</h3>
                              <div class="card-body">
                                   <ul class="list-group">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                             Nueva Partida
                                             <a href="{{route('game.index')}}"><span
                                                       class="badge badge-success badge-pill"><i
                                                            class="fas fa-plus"></i></span></a>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center ">
                                             <div class="row">
                                                  <div class="col-lg-12">
                                                       Unirme a la partida
                                                       <span class="badge badge-primary badge-pill" id="unirme"
                                                            style="cursor: pointer;"><i
                                                                 class="fas fa-external-link-alt"></i></span>
                                                  </div>
                                                  <div class="col-lg-12 mt-1" id="entrar_partida" style="display: none;">
                                                       <input type="text" class="form-control form-control-sm"
                                                            placeholder="Id de la partida">
                                                       <button class="btn btn-sm btn-primary btn-block">Entrar</button>
                                                  </div>
                                             </div>
                                        </li>

                                   </ul>
                                   <p class="text-small text-muted mb-0 pt-3">Realizado por Jhojan Reinoso</p>
                              </div>
                         </div>

                    </div>

                    <div class="col-md-12">
                        
                         <div class="card p-2">
                              <h4 class="text-center">Partidas Jugadas</h4>
                              
                              <div class="card-body">
                                   <table class="table table-sm" id="table">
                                        <thead class="black white-text">
                                             <tr class="text-center">
                                                  <th scope="col">Partida</th>
                                                  <th scope="col">Ronda</th>
                                                  <th scope="col">Ganador</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             @if (!empty($games))
                                             @foreach ($games as $item)
                                            <tr class="text-center">
                                                 <td>{{$item->game_id}}</td>
                                                 <td>{{$item->round}}</td>
                                                 <td>{{$item->winner}}</td>
                                            </tr>
                                             @endforeach
                                             @else
                                             <tr class="text-center">
                                                  <td colspan="3">No hay partidas </td>
                                             </tr>
                                             @endif
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
@include('footer')
<script>
     $(document).ready(function () {
          $("#table").DataTable({
               language:{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Registros del _START_ al _END_",
                    "sInfoEmpty":      "Registros del 0 al 0",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                         "sFirst":    "1°",
                         "sLast":     "Últ",
                         "sNext":     "Sig",
                         "sPrevious": "Ant"
                    },
                    "oAria": {
                         "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                         "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
               },
               pagingType: "full_numbers",
               lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "Todos"]],
          });

          $("#unirme").click(function (e) {
               e.preventDefault();
               $("#entrar_partida").toggle("slow");
          });
     });
</script>
</body>

</html>