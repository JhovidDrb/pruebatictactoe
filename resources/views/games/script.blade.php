<script>
     $(document).ready(function () {
          $("#modalPlayers").modal("show");

          $("#addPlayer").click(function (e) {
               e.preventDefault();
               if ($("#content-player2").hasClass('d-none')) {
                    $("#content-player2").removeClass('d-none');
                    $("#jugador_2").attr("disabled", false);
                    $("#removePlayer").removeClass('d-none');
                    $(this).addClass('d-none');
               }
          });

          $("#removePlayer").click(function (e) {
               e.preventDefault();
               if (!$("#content-player2").hasClass('d-none')) {
                    $("#content-player2").addClass('d-none');
                    $("#jugador_2").attr("disabled", true);
                    $("#addPlayer").removeClass('d-none');
                    $(this).addClass('d-none');
               }
          });

          var boardGame = [];
          var shift = "";

          $(".tictocktoe").click(function () {
               if (boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] == "-") {
                    boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] = shift;
               } else {
                    alert("No puedes jugar en esta posicion");
                    throw "No puedes jugar en esta posicion";
               }

               mark(boardGame, shift);
          });

          $("#form-players").submit(function (e) {
               e.preventDefault();

               $.ajax({
                    type: "POST",
                    url: "{{route('game.newGame')}}",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function (response) {
                         if (response.state == true) {
                              $("#modalPlayers").modal("hide");
                              $("#jugador1").val($("#jugador_1").val());
                              $("#jugador2").val($("#jugador_2").val());
                              let board = "";
                              for (let i = 0; i < response.board.length; i++) {
                                   for (let j = 0; j < response.board[i].length; j++) {
                                        board += "<div class='col-sm-4 border border-dark '>";
                                        board += "<h1 class='text-center tictocktoe font-weight-bold' data-fila='" + i + "' data-column='" + j + "'";
                                        board += "style='cursor: pointer'>" + response.board[i][j] + "</h1></div>";
                                   }
                              }
                              $("#content-board").html(board);
                              $("#id_game").html("ID: " + response.id_game);
                              $("#id_game_input").val(response.id_game);

                              $("#rount_game").html('Ronda: ' + response.rount)
                              $("#rount_game_input").val(response.rount);
                              $("#symbol").html(response.shift);
                              $("#player").html(response.player);
                              shift = response.shift;
                              boardGame = response.board;

                              $(".tictocktoe").click(function () {
                                   if (boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] == "-") {
                                        boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] = shift;
                                   } else {
                                        alert("No puedes jugar en esta posicion");
                                        throw "No puedes jugar en esta posicion";
                                   }
                                   mark(boardGame, shift);
                              });
                         }
                    }
               });
          });

          $("#payNewRound").click(function (e) { 
               e.preventDefault();
               newRound();
          });
     });

     function mark(board, shift) {
          $.ajax({
               type: "GET",
               url: "{{route('game.mark')}}",
               data: {
                    boardGame: board,
                    shift: shift,
                    game: $("#id_game_input").val(),
                    round: $("#rount_game_input").val()
               },
               dataType: "json",
               success: function (response) {
                    if (response.state == true) {
                         $("#modalPlayers").modal("hide");
                         let board = "";
                         for (let i = 0; i < response.board.length; i++) {
                              for (let j = 0; j < response.board[i].length; j++) {
                                   board += "<div class='col-sm-4 border border-dark '>";
                                   board += "<h1 class='text-center tictocktoe font-weight-bold' data-fila='" + i + "' data-column='" + j + "'";
                                   board += "style='cursor: pointer'>" + response.board[i][j] + "</h1></div>";
                              }
                         }
                         $("#content-board").html(board);
                         $("#id_game").html("ID: " + response.id_game);
                         $("#id_game_input").val(response.id_game);

                         $("#rount_game").html('Ronda: ' + response.rount)
                         $("#rount_game_input").val(response.rount);
                         $("#symbol").html(response.shift);
                         $("#player").html(response.player);
                         shift = response.shift;
                         boardGame = response.board;

                         $(".tictocktoe").click(function () {
                              if (boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] == "-") {
                                   boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] = shift;
                              } else {
                                   alert("No puedes jugar en esta posicion");
                                   throw "No puedes jugar en esta posicion";
                              }
                              mark(boardGame, shift);
                         });

                         if (response.result != false) {
                              Swal.fire(
                                   'Finalizo la ronda',
                                   response.result,
                                   'success'
                              );
                              $("#content-board").addClass('disabled_div');
                              $("#content-option").removeClass('d-none');
                         }
                    }
               }
          });
     }

     function newRound() {
          $("#content-board").removeClass('disabled_div');
          $("#content-option").addClass('d-none');
          $.ajax({
               type: "POST",
               url: "{{route('game.newRound')}}",
               headers: {'X-CSRF-TOKEN': $("input[name=_token]").val()},
               data: {
                    game: $("#id_game_input").val(),
               },
               dataType: "json",
               success: function (response) {
                    if (response.state == true) {
                         $("#modalPlayers").modal("hide");
                         let board = "";
                         for (let i = 0; i < response.board.length; i++) {
                              for (let j = 0; j < response.board[i].length; j++) {
                                   board += "<div class='col-sm-4 border border-dark '>";
                                   board += "<h1 class='text-center tictocktoe font-weight-bold' data-fila='" + i + "' data-column='" + j + "'";
                                   board += "style='cursor: pointer'>" + response.board[i][j] + "</h1></div>";
                              }
                         }
                         $("#content-board").html(board);
                         $("#id_game").html("ID: " + response.id_game);
                         $("#id_game_input").val(response.id_game);

                         $("#rount_game").html('Ronda: ' + response.rount)
                         $("#rount_game_input").val(response.rount);
                         $("#symbol").html(response.shift);
                         $("#player").html(response.player);
                         shift = response.shift;
                         boardGame = response.board;

                         $(".tictocktoe").click(function () {
                              if (boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] == "-") {
                                   boardGame[$(this).attr('data-fila')][$(this).attr('data-column')] = shift;
                              } else {
                                   alert("No puedes jugar en esta posicion");
                                   throw "No puedes jugar en esta posicion";
                              }
                              mark(boardGame, shift);
                         });

                         if (response.result != false) {
                              Swal.fire(
                                   'Finalizo la ronda',
                                   response.result,
                                   'success'
                              );
                              $("#content-board").addClass('disabled_div');
                              $("#content-option").removeClass('d-none');
                         }
                    }
               }
          });
     }
</script>