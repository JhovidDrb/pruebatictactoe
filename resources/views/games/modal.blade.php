
<!-- Modal -->
<div class="modal fade" id="modalPlayers" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

      <div class="modal-content">
        <div class="modal-body">
            <h5 class="text-center" id="exampleModalLabel">Jugadores</h5>
            <hr>
            <div class="container">
                <form id="form-players">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="md-form mx-2">
                                <input type="text" id="jugador_1" name="player_1" class="form-control form-control-sm text-center" value="Jugador 1">
                                <label class="text-center" for="jugador_1">Nombre del Jugador 1</label>
                                <small class="form-text text-muted text-center">Representado por "<span class="font-weight-bold">X</span>"</small>
                            </div>
                        </div>
                        
                        <div class="col-md-12" id="content-player2">
                            <div class="md-form mx-2">
                                <input type="text" id="jugador_2" name="player_2" class="form-control form-control-sm text-center" value="Jugador 2">
                                <label class="text-center" for="jugador_2">Nombre del Jugador 2</label>
                                <small class="form-text text-muted text-center">Representado por "<span class="font-weight-bold">O</span>"</small>
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <p class="text-info text-center" id="addPlayer" style="cursor: pointer;"><i class="fas fa-plus"></i> Añadir el 2° Jugador</p>
                            <p class="text-info text-center d-none" id="removePlayer" style="cursor: pointer;"> - Añadir el 2° Jugador</p>
                        </div> -->
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-sm btn-info">Continuar</button>
                    </div>
                </form>
            </div>
        </div>
        
      </div>
    </div>
  </div>