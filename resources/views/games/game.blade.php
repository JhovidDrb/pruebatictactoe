@include('header')
     <div class="container mt-5 mb-5">
          <div class="card">
               <h2 class="card-header info-color white-text text-center py-4">
                    <strong>Tic Tack Toe</strong>
               </h2>
               
               <div class="card-body">
                    <div class="row">
                         <div class="col-md-6 d-flex justify-content-center">
                              <div class="md-form mx-2">
                                   <input type="text" id="jugador1" class="text-center"
                                        value="Jugador 1" disabled>
                              </div>
                         </div>

                         <div class="col-md-6 d-flex justify-content-center">
                              <div class="md-form mx-2">
                                   <input type="text" id="jugador2" class="text-center" disabled
                                        value="Jugador 2">
                              </div>
                         </div>

                         <div class="col-md-12 m-0 p-0">
                              <div class="d-flex justify-content-center mt-5">
                                   <div class="card p-5">
                                        <h4 class="text-center">Tu representas el simbolo "<span class="font-weight-bold" id="symbol">-</span>"</h4>
                                        <h5 class="text-center">Es el turno de <span id="player">X</span></h5>
                                        <hr>
                                        <h4 class="text-center" id="id_game">ID: - </h4>
                                        <input type="hidden" id="id_game_input">

                                        <h5 class="text-center" id="rount_game">Ronda: - </h5>
                                        <input type="hidden" id="rount_game_input">
                                        
                                        <div class="card-body">
                                             <div class="row d-flex justify-content-center" id="content-board">
                                                  @foreach ($board as $fila => $item)
                                                  @foreach ($item as $column => $val)
                                                  <div class="col-sm-4 border border-dark ">
                                                       <h1 class="text-center tictocktoe font-weight-bold" data-fila="{{$fila}}" data-column="{{$column}}"
                                                            style="cursor: pointer"> - </h1>
                                                  </div>
                                                  @endforeach
                                                  @endforeach
                                             </div>
                                        </div>
                                   </div>

                              </div>
                         </div>
                         
                         <div class="col-md-12 d-none" id="content-option">
                              <div class="d-flex justify-content-center m-1">
                                   <button class="btn btn-sm btn-info" id="payNewRound">Jugar otra ronda <i class="fas fa-redo"></i></button>
                                   <a href="{{route('game.index')}}" class="btn btn-sm btn-success" id="payNewGame">Jugar otra partida <i class="fas fa-plus"></i></a>
                                   <a href="{{url('/')}}" class="btn btn-sm btn-light">Pagina Principal <i class="fas fa-home"></i></a>
                              </div>
                         </div>
                    </div>
               </div>

          </div>
     </div>

     @include('games.modal')
     @include('footer')
     @include('games.script')