<?php

use App\Http\Controllers\GameController;
use App\Models\Game;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $games = Game::select('rounds_games.id as rounds_games_id', 'rounds_games.game_id','rounds_games.round','rounds_games.winner')
            ->join('rounds_games', 'games.id', 'rounds_games.game_id')
            ->orderBy('rounds_games.id', 'desc')->get();
    return view('home',compact('games'));
});

Route::prefix('game')->group(function () {
    Route::get('', [GameController::class, 'index'])->name('game.index');
    Route::post('newGame', [GameController::class, 'newGame'])->name('game.newGame');
    Route::get('mark', [GameController::class, 'mark'])->name('game.mark');
    Route::post('newRound', [GameController::class, 'newRound'])->name('game.newRound');
});
