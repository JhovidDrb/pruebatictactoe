<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoundGame extends Model
{
    use HasFactory;

    protected $table = "rounds_games";
    
    protected $casts = [
        'board' => 'array',
    ];

    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id');
    }
}
