<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\RoundGame;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $board = [
            0 => ["", "", ""],
            1 => ["", "", ""],
            2 => ["", "", ""]
        ];

        return view('games.game', compact('board'));
    }

    /**
     * Crea una nueva partida.
     *
     * @param  \Illuminate\Http\Request  $request['player_1'],$request['player_2'],
     * @return \Illuminate\Http\Response
     */
    public function newGame(Request $request)
    {
        $newGame = new Game;
        $newGame->first_player = $request->player_1;
        $newGame->second_player = $request->player_2;

        $newGame->save();

        $round = new RoundGame();
        $round->game_id = $newGame->id;
        $round->round = 1;
        $round->board =  [
            0 => ["-", "-", "-"],
            1 => ["-", "-", "-"],
            2 => ["-", "-", "-"]
        ];
        $round->shift = 0;

        $res = $round->save();

        if ($res) {
            $id_game = $newGame->id;

            return response()->json([
                'state' => true,
                'id_game' => $id_game,
                'rount' => $round->round,
                'board' => $round->board,
                'shift' => "X",
                'player' => $request->player_1,
                'result' => false,
            ]);
        }
    }

    /**
     * Registra el movimiento de la partida.
     *
     * @param  \Illuminate\Http\Request  $request['game'],$request['round'],$request['boardGame'],$request['shift']
     * @return \Illuminate\Http\Response
     */
    public function mark(Request $request)
    {
        //    dd($request->all(),$request['round'],$request['game']);
        $game = Game::select('games.first_player', 'games.second_player', 'rounds_games.id as rounds_games_id', 'rounds_games.game_id', 'rounds_games.round', 'rounds_games.board', 'rounds_games.shift')
            ->where('games.id', $request['game'])
            ->where('rounds_games.round', $request['round'])
            ->join('rounds_games', 'games.id', 'rounds_games.game_id')
            ->orderBy('rounds_games.id', 'desc')->first();

        $round = RoundGame::find($game->rounds_games_id);
 
        $round->board = $request['boardGame'];

        if ($request['shift'] == "X") {
            $shift = "O";
            $player = $game->second_player;
            $game->shift = 2;
        } else {
            $shift = "X";
            $player = $game->first_player;
            $round->shift = 1;
        }

        $res = $round->save();
        $result = false;
        $end = [];
        for ($i=0; $i < count($round->board); $i++) { 

            if (
                $round->board[$i][0] == "X" && $round->board[$i][1] == "X" && $round->board[$i][2] == "X" ||
                $round->board[0][$i] == "X" && $round->board[1][$i] == "X" && $round->board[2][$i] == "X" ||
                $round->board[0][0] == "X" && $round->board[1][1] == "X" && $round->board[2][2] == "X"  ||
                $round->board[2][0] == "X" && $round->board[1][1] == "X" && $round->board[0][2] == "X"  
            ) {
                $result = "¡Felicidades gana ". $game->first_player." ( X )!";
                $round->shift = 1;
                $round->winner = $game->first_player;
                $player = $game->first_player;
                $round->save();
            } else 
            if (
                $round->board[$i][0] == "O" && $round->board[$i][1] == "O" && $round->board[$i][2] == "O" ||
                $round->board[0][$i] == "O" && $round->board[1][$i] == "O" && $round->board[2][$i] == "O" ||
                $round->board[0][0] == "O" && $round->board[1][1] == "O" && $round->board[2][2] == "O"  ||
                $round->board[2][0] == "O" && $round->board[1][1] == "O" && $round->board[0][2] == "O"  
            ) {
                $result = "¡Felicidades gana ". $game->second_player." ( 0 )!";
                $round->shift = 2;
                $round->winner = $game->second_player;
                $player = $game->second_player;
                $round->save();
            }

            if( in_array("-",$round->board[$i]) ){
                continue;
            }else{
                array_push($end,true);
            }
        }
        if(count($end) >= 3){
            $result = "¡Empate!";
        }

        if ($res) {

            return response()->json([
                'state' => true,
                'id_game' => $game->game_id,
                'rount' => $round->round,
                'board' => $round->board,
                'shift' => $shift,
                'player' => $player,
                'result' => $result
            ]);
        }
    }

    /**
     * Crea una nueva ronda en la partida.
     *
     * @param  \Illuminate\Http\Request  $request['game']
     * @return \Illuminate\Http\Response
     */
    public function newRound(Request $request)
    {   
        $game = Game::select('games.first_player','games.second_player','rounds_games.id as rounds_games_id', 'rounds_games.game_id', 'rounds_games.round', 'rounds_games.shift')
        ->where('games.id', $request['game'])
        ->join('rounds_games', 'games.id', 'rounds_games.game_id')
        ->orderBy('rounds_games.id', 'desc')->first();

        $round = $game->round + 1;
        if ($round %2 == 0) {
            $shift = "O";
            $player = $game->second_player;           
        } else {
            $shift = "X";
            $player = $game->first_player;
        }
        $newRound = new RoundGame();
        $newRound->game_id = $game->game_id;
        $newRound->round =  $round;
        $newRound->board = [
            0 => ["-", "-", "-"],
            1 => ["-", "-", "-"],
            2 => ["-", "-", "-"]
        ];
        $newRound->shift = $shift;
        $res = $newRound->save();
        if($res){
            return response()->json([
                'state' => true,
                'id_game' => $game->game_id,
                'rount' => $newRound->round,
                'board' => $newRound->board,
                'shift' => $newRound->shift,
                'player' => $player,
                'result' => false
            ]);
        }
    }
    
}
